import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";

import BootstrapVue from "bootstrap-vue/dist/bootstrap-vue.esm";
// import { ValidationProvider, extend } from "vee-validate";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(BootstrapVue);
Vue.use(VueRouter);
// Vue.use(Vuelidate);

Vue.config.productionTip = false;

import Orders from "./components/Orders.vue";
import Products from "./components/Products.vue";
import Posts from "./components/Posts.vue";

const router = new VueRouter({
  routes: [
    { path: "/orders", component: Orders },
    { path: "/products", component: Products },
    { path: "/posts", component: Posts },
  ],
});

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
